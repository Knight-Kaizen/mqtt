
const mqtt = require('mqtt');

// const client = mqtt.connect('mqtt://broker.hivemq.com');
const client = mqtt.connect('mqtt://localhost:1883');
const client2 = mqtt.connect('mqtt://localhost:1883');

client.on('connect', ()=>{
    client.subscribe('topic name');
    console.log('client 1 has subscribed successfully');
});
client2.on('connect', ()=>{
    client2.subscribe('temperature gt 30');
    console.log('client 2 has subscribed successfully');
});


client.on('message', (topic, message)=>{
    console.log('message received by client 1', message.toString());
})
client2.on('message', (topic, message)=>{
    console.log('message received by client 2', message.toString());
})