const mqtt = require('mqtt');

// const client = mqtt.connect('mqtt://broker.hivemq.com');
const client = mqtt.connect('mqtt://localhost:1883');
client.on("connect", ()=>{
    setInterval(()=>{
        var randomTemperature = Math.random()*50;
        console.log(randomTemperature);
        if(randomTemperature < 30){
            client.publish('topic name', 'this is topic message ' + randomTemperature.toString());
        }
        if(randomTemperature >= 30){
            client.publish('temperature gt 30', 'this is for client 2 ' + randomTemperature.toString());
        }
    }, 3000);
}); 